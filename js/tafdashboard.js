google.load('visualization', '1', {
	packages: ['corechart']
});

var linkList = {
	development: {
		serverName: 'DMW Development',
		chartType: 'pie',
		url: encodeURIComponent('https://bestseller.jira.com/builds/rest/api/latest/result/TAF-BIF-TEST/latest.json?os_username=apiuser&os_password=60ac27185bbb54cc989b03cc82e081c5'),
		canvasId: 'piechart_dev'
	},
	staging: {
		serverName: 'DMW Staging',
		chartType: 'pie',
		url: encodeURIComponent('https://bestseller.jira.com/builds/rest/api/latest/result/TAF-TAFES/latest.json?os_username=apiuser&os_password=60ac27185bbb54cc989b03cc82e081c5'),
		canvasId: 'piechart_stg'
	},
	production: {
		serverName: 'DMW Production',
		chartType: 'pie',
		url: encodeURIComponent('https://bestseller.jira.com/builds/rest/api/latest/result/TAF-BIF-TESTDWPRODUCTION/latest.json?os_username=apiuser&os_password=60ac27185bbb54cc989b03cc82e081c5'),
		canvasId: 'piechart_prd'
	},
	developmentYear: {
		serverName: 'DMW Development',
		chartType: 'combo',
		url: encodeURIComponent('https://bestseller.jira.com/builds/rest/api/latest/result/TAF-BIF-TEST.json?os_username=apiuser&os_password=60ac27185bbb54cc989b03cc82e081c5&expand=results.result&max-result=1500'),
		canvasId: 'combochart_dev'
	},
	stagingYear: {
		serverName: 'DMW Staging',
		chartType: 'combo',
		url: encodeURIComponent('https://bestseller.jira.com/builds/rest/api/latest/result/TAF-TAFES.json?os_username=apiuser&os_password=60ac27185bbb54cc989b03cc82e081c5&expand=results.result&max-result=1500'),
		canvasId: 'combochart_stg'
	}
}
var lastTimeStamp;

google.setOnLoadCallback(function() {
	$.each(linkList, function(i, linkItem) {
		$.ajax({
			type: 'GET',
			cache: false,
			url: 'proxy.php?url=' + linkItem.url,
			dataType: 'json',
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Authorization', 'Basic ' + getParameterByName('key'));
			},
			success: function(data) {
				if (linkItem.chartType === 'combo' && data.status.http_code === 200) {
					renderCombochart(data.contents, linkItem.canvasId);
				} else if (linkItem.chartType === 'pie' && data.status.http_code === 200) {
					renderPiechart(data.contents, linkItem.canvasId);
				} else {
					$('#' + linkItem.canvasId).addClass('coming-soon');
					$('#' + linkItem.canvasId).prepend('<h2>' + linkItem.serverName + '</h2>');
				}
			},
			error: function() {
				renderError();
			}
		});
	});
	renderLastTimeStamp();
});

Date.prototype.getWeek = function() {
    var d = new Date(this);
    d.setHours(0,0,0);
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay()||7));
    // Get first day of year
    var yearStart = new Date(d.getFullYear(),0,1);
    // Calculate full weeks to nearest Thursday
    return Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
}

Date.prototype.getWeeksInYear = function() {
	var d = new Date(this.getFullYear(), 11, 31);
	var week = d.getWeek();
	return week == 1 ? (new Date(d.setDate(24))).getWeek() : week;
}

Date.prototype.getDateWithYear = function(year) {
	var d = new Date(this);
	d.setFullYear(year);
	return d;
}

function getParameterByName(name) {
	name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
		results = regex.exec(location.search);
	return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function renderError() {
	$('.js-render-error').html('<h3>UNDER MAINTENANCE</h3><p>We will be back online soon...</p>');
	$('.graph-group > div').css('background', 'none');
}

function renderPiechart(graphData, graphObjectId) {
	var data = google.visualization.arrayToDataTable([
		['Test type', 'No. of tests'],
		['Successful', graphData.successfulTestCount],
		['Failed', graphData.failedTestCount],
		['Skipped', graphData.skippedTestCount],
		['Quarantined', graphData.quarantinedTestCount]
	]),
		options = {
		is3D: true,
		backgroundColor: 'transparent',
		fontName: 'adelle-light',
		legend: {position: 'none'},
		pieSliceText: 'percentage',
		pieSliceTextStyle: {color: 'black', fontSize: 18, bold: 1},
		slices: {
			0: {color: '#EAEBEB', textStyle: {color: 'darkgreen'}},
			1: {color: '#525253', offset: 0.1, textStyle: {color: 'darkred'}},
			2: {color: '#CCCCCC', textStyle: {color: 'darkorange'}},
			3: {color: '#A1A2A3'}
		}
	},
		chart = new google.visualization.PieChart(document.getElementById(graphObjectId));

	chart.draw(data, options);

	$('#' + graphObjectId).prepend(
		'<div class="test-summary">' + 
		'<h2>' + graphData.planName + ' <span># ' + graphData.buildResultKey + '</span></h2>' +
		'<span>' + graphData.buildTestSummary + '</span>, it ran <span>' + graphData.buildRelativeTime + '</span> and took <span>' + graphData.buildDurationDescription + '</span> to complete.' + 
		'</div>'
	);
	$('#' + graphObjectId).css('background', 'none');
}

function renderCombochart(graphData, graphObjectId) {
	if (graphData && graphData.results && graphData.results.result.length > 0) {
		var planName = graphData.results.result[0].planName,
			columns = [['Week', 'Successful', 'Failed', 'Skipped', 'Quarantined', 'Percentile']],
			data = google.visualization.arrayToDataTable(_.union(columns, extractGraphData(graphData))),
			options = {
				backgroundColor: 'transparent',
				fontName: 'adelle-light',
				legend: {textStyle: {color: '#525252', fontSize: 16, bold: 1}},
				bar: {groupWidth: '90%'},
				isStacked: true,
				seriesType: "bars",
				series: {
					0: {color: '#EAEBEB'},
					1: {color: '#525253'},
					2: {color: '#CCCCCC'},
					3: {color: '#A1A2A3'},
					4: {color: 'black', type: 'line', targetAxisIndex: 1, lineWidth: 1, visibleInLegend: false}
				},
				hAxis: {title: planName + ' - Weeknumber', titleTextStyle: {color: '#525252', fontSize: 16, bold: 1},  showTextEvery: 1, textStyle: {fontSize: 12}},
				vAxes: {
					0: {title:'# tests per week', titleTextStyle: {color: '#525252', fontSize: 16, bold: 1}},
					1: {title:'% successful per week', titleTextStyle: {color: '#525252', fontSize: 16, bold: 1}}
				},
				animation: {
					duration: 100,
					easing: 'out',
					startup: true
				}
			},
			chart = new google.visualization.ComboChart(document.getElementById(graphObjectId));
		
		chart.draw(data, options);
	}

	$('#' + graphObjectId).css('background', 'none');
}

function renderLastTimeStamp() {
	if (lastTimeStamp) {
		$('.timestamp').html(lastTimeStamp.description);
	}
}

function timedRefresh(timeoutPeriod) {
	// 1800000 milliseconds = 30 minutes
	setTimeout('location.reload(true);', timeoutPeriod);
}

function extractGraphData(graphData) {
	var today = new Date(),
		currentWeek = today.getWeek(),
		weeksInYear = today.getWeeksInYear(),
		weeksInPreviousYear = today.getDateWithYear(today.getFullYear() - 1).getWeeksInYear(),
		timeStamp = new Date(graphData.results.result[0].buildCompletedDate);

	if (!lastTimeStamp || lastTimeStamp.date < timeStamp) {
		lastTimeStamp = {date: timeStamp, description: graphData.results.result[0].prettyBuildCompletedTime};
	}

	// group by weeks
	var data = _.groupBy(graphData.results.result, function(result) {
			return (new Date(result.buildCompletedDate)).getWeek();
		});

	// only get data up to a year
	if (data.length > weeksInYear) {
		data = _.first(data, weeksInYear);
	}

	// sum up the weekly data
	data = _.map(data, function(weekData, key) {
		return [
			key,
			_(weekData).reduce(function(sum, add) { return sum + add.successfulTestCount; }, 0),
			_(weekData).reduce(function(sum, add) { return sum + add.failedTestCount; }, 0),
			_(weekData).reduce(function(sum, add) { return sum + add.skippedTestCount; }, 0),
			_(weekData).reduce(function(sum, add) { return sum + add.quarantinedTestCount; }, 0)
		];
	});

	// add the percentage to the weekly data
	for (var i = 0; i < data.length; i++) {
		data[i].push(
			Math.round(((data[i][1] + data[i][3]) / (_.reduce(_.rest(data[i]), function(sum, add) {
				return sum + add;
			}, 0))) * 100)
		);
	};

	// Join actual data with empty placeholders
	var joinedData = [],
		weekNumber = currentWeek + 1;
	for (var i = 0; i < weeksInPreviousYear; i++) {
		if (weekNumber > weeksInPreviousYear) {
			weekNumber = 1;
		}

		var dataItem = _.find(data, function(item) { return item[0] == weekNumber.toString(); });
		if (typeof dataItem != 'undefined') {
			joinedData.push(dataItem);
		} else {
			joinedData.push([weekNumber.toString(), 0, 0, 0, 0, 0]);
		}

		weekNumber++;
	}

	return joinedData;
}
